from occam.commands.manager import command, option, argument

from occam.log    import Log

from occam.manager import uses

from occam.objects.manager    import ObjectManager

@command('my-component', 'my-command')
@uses(ObjectManager)
class MyCommand:
  def do(self):
    Log.write("ran this command.")
    return 0
